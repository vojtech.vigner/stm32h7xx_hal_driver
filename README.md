# STM32H7xx HAL Driver

The STM32Cube hardware abstraction layer (HAL) and Low Layer APIs (LL) for STM32H7 family with CMSIS library. 

**This is reduced version of otherwise bulky library which comes with STM32CubeMX software.** 

This library is compatible with [PlatformIO Library Management System](https://docs.platformio.org/en/latest/librarymanager/creating.html).

For use with platformio you can add library in platformio.ini like this:
>[env]
>platform = ststm32
>
>lib_deps = 
>&nbsp;&nbsp;&nbsp;&nbsp;stm32h7xx_hal_driver = https://gitlab.com/vojtech.vigner/stm32h7xx_hal_driver.git

Don't forget to select proper linker script if not selected by *board* variable. 
>board_build.ldscript = "mylinker.ld"

Add startup code to your project, startup examples are located in templates/*compiler* folder. These startup examples calls *SystemInit* function before *main*. 

*SystemInit* is used to setup CPU clocks and configure Interrupt Vector Table. Again examples are located in *templates/system* folder.

Code location in STM32CubeMX library folder:
- **include**: *Drivers/STM32H7xx_HAL_Driver/Inc*
- **include/cmsis**: *Drivers/CMSIS/Include*
- **include/device**: *Drivers/CMSIS/Device/ST/STM32H7xx/Include*
- **src**: *Drivers/STM32H7xx_HAL_Driver/Src*
- **templates**: *Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_\*_template.c*
- **templates/arm**: *Drivers/CMSIS/Device/ST/STM32H7xx/Source/Templates/arm*
- **templates/gcc**: *Drivers/CMSIS/Device/ST/STM32H7xx/Source/Templates/gcc*
- **templates/iar**: *Drivers/CMSIS/Device/ST/STM32H7xx/Source/Templates/iar*
- **templates/system**: *Drivers/CMSIS/Device/ST/STM32H7xx/Source/Templates*

# Original STM README:

# STM32CubeH7 MCU Firmware Package

**STM32Cube** is an STMicroelectronics original initiative to ease the developers life by reducing efforts, time and cost. 

**STM32Cube** covers the overall STM32 products portfolio. It includes a comprehensive embedded software platform (this repo), delivered for each series (such as the STM32CubeH7 for the STM32H7 series).
   * The CMSIS modules (core and device) corresponding to the ARM-tm core implemented in this STM32 product
   * The STM32 HAL-LL drivers : an abstraction drivers layer, the API ensuring maximized portability across the STM32 portfolio
   * The BSP Drivers of each evaluation or demonstration board provided by this STM32 series
   * A consistent set of middlewares components such as RTOS, USB, FatFS, LwIP, Graphics ...
   * A full set of software projects (basic examples, applications or demonstrations) for each board provided by this STM32 series

The **STM32CubeH7 MCU Package** projects are directly running on the STM32H7 series boards. You can find in each Projects/*Board name* directories a set of software projects (Applications/Demonstration/Examples)

In this FW Package, the modules **Middlewares/ST/TouchGFX** **Middlewares/ST/STemWin** **Middlewares/ST/STM32_Audio** are not directly accessible. They must be downloaded from a ST server, the respective URL are available in a readme.txt file inside each module.

## Boards available
  * STM32H7
    * [NUCLEO-H7A3ZI-Q](https://www.st.com/en/product/nucleo-h7a3zi-q.html)
    * [NUCLEO-H723ZG](https://www.st.com/en/product/nucleo-h723zg.html)
    * [NUCLEO-H743ZI](https://www.st.com/en/product/nucleo-h743zi.html)
    * [NUCLEO-H743ZI2](https://www.st.com/en/product/nucleo-h743zi.html)
    * [NUCLEO-H753ZI](https://www.st.com/en/product/nucleo-h753zi.html)
    * [NUCLEO-H753ZI2](https://www.st.com/en/product/nucleo-h753zi.html)
    * [NUCLEO-H745ZI-Q](https://www.st.com/en/product/nucleo-h745zi-q.html)
    * [NUCLEO-H755ZI-Q](https://www.st.com/en/product/nucleo-h755zi-q.html)
    * [STM32H7B3I-DK](https://www.st.com/en/product/stm32h7b3i-dk.html)
    * [STM32H7B3I-EVAL](https://www.st.com/en/product/stm32h7b3i-eval.html)
    * [STM32H735G-DK](https://www.st.com/en/product/stm32h735g-dk.html)
    * [STM32H743I-EVAL](https://www.st.com/en/product/stm32h743i-eval.html)
    * [STM32H743I-EVAL2](https://www.st.com/en/product/stm32h743i-eval.html)
    * [STM32H753I-EVAL](https://www.st.com/en/product/stm32h753i-eval.html)
    * [STM32H753I-EVAL2](https://www.st.com/en/product/stm32h753i-eval.html)
    * [STM32H745I-DISCO](https://www.st.com/en/product/stm32h745i-disco.html)
    * [STM32H747I-DISCO](https://www.st.com/en/product/stm32h747i-disco.html)
    * [STM32H747I-DISC1](https://www.st.com/en/product/stm32h747i-disco.html)
    * [STM32H747I-EVAL](https://www.st.com/en/product/stm32h747i-eval.html)
    * [STM32H757I-EVAL](https://www.st.com/en/product/stm32h757i-eval.html)
    * [STM32H750B-DK](https://www.st.com/en/product/stm32h750b-dk.html)

## Troubleshooting

**Caution** : The **Issues** requests are strictly limited to submit problems or suggestions related to the software delivered in this repo

**For any other question** related to the STM32H7 product, the hardware performance, the hardware characteristics, the tools, the environment, you can submit a topic on the [ST Community/STM32 MCUs forum](https://community.st.com/s/group/0F90X000000AXsASAW/stm32-mcus)
